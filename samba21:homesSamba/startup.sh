#! /bin/bash

useradd -m -s /bin/bash unix01
useradd -m -s /bin/bash unix02
useradd -m -s /bin/bash unix03
echo -e "unix01\nunix01" | passwd unix01
echo -e "unix02\nunix02" | passwd unix02
echo -e "unix03\nunix03" | passwd unix03

cp /opt/docker/login.defs /etc/login.defs
cp /opt/docker/nscd.conf /etc/nscd.conf
cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/pam_ldap.conf /etc/pam_ldap.conf
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/smb.conf /etc/samba/smb.conf
chmod 0600 /etc/nsswitch.conf

/etc/init.d/nslcd start
/etc/init.d/nscd start

#------------------------//
# Crear els homes dels usuaris de LDAP (crear-omplir-chown)
mkdir /tmp/home
mkdir /tmp/home/pere
mkdir /tmp/home/pau
mkdir /tmp/home/anna
mkdir /tmp/home/marta
mkdir /tmp/home/jordi
mkdir /tmp/home/admin
cp /opt/docker/README.md /tmp/home/pere
cp /opt/docker/README.md /tmp/home/pau
cp /opt/docker/README.md /tmp/home/anna
cp /opt/docker/README.md /tmp/home/marta
cp /opt/docker/README.md /tmp/home/jordi
cp /opt/docker/README.md /tmp/home/admin
chown -R pere.users /tmp/home/pere
chown -R pau.users /tmp/home/pau
chown -R anna.alumnes /tmp/home/anna
chown -R marta.alumnes /tmp/home/marta
chown -R jordi.users /tmp/home/jordi
chown -R admin.wheel /tmp/home/admin
# Usuaris locals super3 unix i samba
useradd patipla
useradd lila
useradd roc
useradd pla
echo -e "patipla\npatipla" | smbpasswd -a patipla
echo -e "lila\nlila" | smbpasswd -a lila
echo -e "roc\nroc" | smbpasswd -a roc
echo -e "pla\npla" | smbpasswd -a pla
# Crear els comptes Samba dels usuaris LDAP
echo -e "pere\npere" | smbpasswd -a pere
echo -e "pau\npau" | smbpasswd -a pau
echo -e "anna\nanna" | smbpasswd -a anna
echo -e "marta\nmarta" | smbpasswd -a marta
echo -e "jordi\njordi" | smbpasswd -a jordi
echo -e "admin\nadmin" | smbpasswd -a admin


/usr/sbin/smbd -F
